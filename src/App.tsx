import { useForm, SubmitHandler, FormProvider } from "react-hook-form";
import classNames from "classnames";
import TextAreaField from "./components/TextAreaField";

type formProps = {
  text: string;
  longText: string;
};

function App() {
  const formModel = useForm<formProps>();
  const {
    register,
    handleSubmit,
    formState: { errors: formErrors },
  } = formModel;
  const onSubmit: SubmitHandler<formProps> = (data) =>
    // eslint-disable-next-line no-alert
    window.alert(
      Object.entries(data)
        .map((x) => x.join(":"))
        .join("\r\n")
    );

  return (
    <div className="App container pt-5">
      <div className="row form-container">
        <FormProvider {...formModel}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-floating mb-4">
              <input
                className={classNames({
                  "form-control": true,
                  "is-invalid": formErrors.text,
                })}
                id="text"
                type="text"
                autoComplete="off"
                placeholder="Please input text"
                {...register("text", { required: true, maxLength: 10 })}
              />
              <label htmlFor="text" className="col col-form-label text-center">
                {(formErrors.text?.type === "required" && "Text is required") ||
                  (formErrors.text?.type === "maxLength" && "Too much") ||
                  "Input text"}
              </label>
            </div>
            <TextAreaField name="longText" maxLength={150} />
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </FormProvider>
      </div>
    </div>
  );
}

export default App;
