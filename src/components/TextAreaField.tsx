import classNames from "classnames";
import {
  FunctionComponent,
  useRef,
  ChangeEvent,
  useState,
  useEffect,
} from "react";
import { useFormContext } from "react-hook-form";

const TextAreaField: FunctionComponent<{ name: string; maxLength: number }> =
  function TextArea({ name, maxLength }) {
    const {
      register,
      setValue,
      getValues,
      formState: { errors: formErrors },
    } = useFormContext();

    const handleChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
      e.target.style.cssText = `height:auto`;
      e.target.style.cssText = `height:${e.target.scrollHeight + 2}px`;
    };

    const textareaRef = useRef<HTMLTextAreaElement | null>(null);
    const { ref, ...rest } = register(name, {
      required: true,
      maxLength,
      onChange: handleChange,
    });

    const [onInput, setOnInput] = useState(false); // for handle input Chinese

    const handleInput = () => {
      if (!textareaRef.current || !maxLength) return;
      if (!onInput && textareaRef.current.textLength > maxLength)
        setValue(name, textareaRef.current.value.slice(0, maxLength), {
          shouldValidate: true,
        });
    };

    const onCompositionstart = () => {
      setOnInput(true);
    };
    const onCompositionend = () => {
      setOnInput(false);
      if (
        textareaRef.current &&
        maxLength &&
        textareaRef.current.textLength > maxLength
      )
        setValue(name, textareaRef.current.value.slice(0, maxLength), {
          shouldValidate: true,
        });
    };
    const onKeyup = (e: KeyboardEvent) => {
      if (maxLength && e.key === "Enter") {
        const cutText = getValues(name).slice(0, maxLength);
        setValue(name, cutText, {
          shouldValidate: true,
        });
      }
    };
    useEffect(() => {
      if (textareaRef.current) {
        const { scrollHeight } = textareaRef.current;
        textareaRef.current.style.height = `${scrollHeight + 2}px`;
      }
      textareaRef.current?.addEventListener(
        "compositionstart",
        onCompositionstart
      );
      textareaRef.current?.addEventListener("compositionend", onCompositionend);
      textareaRef.current?.addEventListener("keyup", onKeyup);
      return () => {
        textareaRef.current?.removeEventListener(
          "compositionstart",
          onCompositionstart
        );
        textareaRef.current?.removeEventListener(
          "compositionend",
          onCompositionend
        );
        textareaRef.current?.removeEventListener("keyup", onKeyup);
      };
    }, []);

    return (
      <div className="form-floating mb-4">
        <textarea
          style={{ resize: "none", overflow: "hidden" }}
          className={classNames({
            "form-control": true,
            "is-invalid": formErrors[name],
          })}
          id="textArea"
          autoComplete="off"
          placeholder="Please input a lot text"
          rows={1}
          onInput={handleInput}
          {...rest}
          ref={(e: null) => {
            ref(e);
            textareaRef.current = e;
          }}
        />
        <label htmlFor="textArea" className="col col-form-label text-center">
          {(formErrors[name]?.type === "required" && "Text is required") ||
            (formErrors[name]?.type === "maxLength" && "Too much") ||
            "Input text"}
        </label>
      </div>
    );
  };

export default TextAreaField;
